This is Solidaric, a minimal theme for [Bludit](https://github.com/bludit/bludit), based upon μ.

What do you end up with when you strip away ~~the noise~~ everything?

- Single PHP file for entry listing and entry page. Categories are only mentioned but not used. Tags are unused.
- Minimal CSS.
- Fast.
- Dark mode as well.
- Solidaric font with fancy italics.
